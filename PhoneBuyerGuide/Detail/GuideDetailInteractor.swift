//
//  GuideDetailInteractor.swift
//  PhoneBuyerGuide
//
//  Created by Khanit on 29/5/2562 BE.
//  Copyright (c) 2562 SCB. All rights reserved.
//

protocol GuideDetailInteractorInterface {
    func fetchPhoneDetail(with request: GuideDetail.Something.Request)
    var model: [ImageItem]? { get }
}

final class GuideDetailInteractor: GuideDetailInteractorInterface {
    var presenter: GuideDetailPresenterInterface!
    var worker: GuideDetailWorker?
    var model: [ImageItem]?
    
    // MARK: - Business logic
    
    func fetchPhoneDetail(with request: GuideDetail.Something.Request) {
        worker?.fetchPhoneDetail(with: request.phoneId) { [weak self] in
            if case let Result.success(data) = $0 {
                self?.model = data
            } else if case Result.failure(_) = $0 {
                // present error alert message
                return
            }
            let response = GuideDetail.Something.Response(images: self?.model)
            self?.presenter.presentImages(response: response)
        }
    }
}

