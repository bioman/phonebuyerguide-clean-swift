//
//  GuideDetailModels.swift
//  PhoneBuyerGuide
//
//  Created by Khanit on 29/5/2562 BE.
//  Copyright (c) 2562 SCB. All rights reserved.
//

import UIKit

struct GuideDetail {
  /// This structure represents a use case
  struct Something {
    /// Data struct sent to Interactor
    struct Request {
        let phoneId: Int64
    }
    /// Data struct sent to Presenter
    struct Response {
        let images: [ImageItem]?
    }
    /// Data struct sent to ViewController
    struct ViewModel {}
  }
}
