//
//  GuideDetailPresenter.swift
//  PhoneBuyerGuide
//
//  Created by Khanit on 29/5/2562 BE.
//  Copyright (c) 2562 SCB. All rights reserved.
//

protocol GuideDetailPresenterInterface {
    func presentImages(response: GuideDetail.Something.Response)
}

final class GuideDetailPresenter: GuideDetailPresenterInterface {
    
    weak var viewController: GuideDetailViewControllerInterface!
    
    // MARK: - Presentation logic

    func presentImages(response: GuideDetail.Something.Response) {
        viewController.setupImageViews(with: response.images)
    }
}

