//
//  GuideDetailRouter.swift
//  PhoneBuyerGuide
//
//  Created by Khanit on 29/5/2562 BE.
//  Copyright (c) 2562 SCB. All rights reserved.
//

import UIKit

protocol GuideDetailRouterInput {
    
}

final class GuideDetailRouter: GuideDetailRouterInput {
    weak var viewController: GuideDetailViewController!
    
}

