//
//  GuideDetailStore.swift
//  PhoneBuyerGuide
//
//  Created by Khanit on 29/5/2562 BE.
//  Copyright (c) 2562 SCB. All rights reserved.
//

import Foundation

final class GuideDetailStore: GuideDetailStoreProtocol {
    var service: PhoneService?
    
    func fetchPhoneDetail(with phoneId: Int64, completion: @escaping PhoneDetailServiceCompletion) {
        let parameter = PhoneDetailRequestParameter(mobileId: phoneId)
        service?.fetchPhoneDetail(parameter: parameter, completion: completion)
    }
}

