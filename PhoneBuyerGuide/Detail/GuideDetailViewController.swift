//
//  GuideDetailViewController.swift
//  PhoneBuyerGuide
//
//  Created by Khanit on 29/5/2562 BE.
//  Copyright (c) 2562 SCB. All rights reserved.
//

import UIKit

protocol GuideDetailViewControllerInterface: class {
    func setupImageViews(with items: [ImageItem]?)
}

final class GuideDetailViewController: UIViewController, GuideDetailViewControllerInterface {
    
    @IBOutlet private weak var imageScrollView: UIScrollView!
    @IBOutlet private weak var priceLabel: UILabel!
    @IBOutlet private weak var ratingLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UITextView!
    
    var interactor: GuideDetailInteractorInterface!
    var router: GuideDetailRouter!
    var phoneCellViewModel: PhoneCellViewModel?
    
    // MARK: - Object lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configure(viewController: self)
    }
    
    // MARK: - Configuration
    
    private func configure(viewController: GuideDetailViewController) {
        let router = GuideDetailRouter()
        router.viewController = viewController
        
        let presenter = GuideDetailPresenter()
        presenter.viewController = viewController

        let interactor = GuideDetailInteractor()
        interactor.presenter = presenter

        let store = GuideDetailStore()
        store.service = PhoneServiceImplementation()
        
        interactor.worker = GuideDetailWorker(store: store)
        
        viewController.interactor = interactor
        viewController.router = router
    }
    
    // MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    // MARK: - Event handling
    
    func setupView() {
        guard let phoneCellViewModel = phoneCellViewModel else { return }
        configureViews(with: phoneCellViewModel)
        let request = GuideDetail.Something.Request(phoneId: phoneCellViewModel.phoneId)
        interactor.fetchPhoneDetail(with: request)
    }
    
    // MARK: - Display logic
    
    func setupImageViews(with items: [ImageItem]?) {
        guard let items = items else { return }
        DispatchQueue.main.async {
            self.configureImageViews(with: items)
        }
    }
}

private extension GuideDetailViewController {
    func configureViews(with cellViewModel: PhoneCellViewModel) {
        title = cellViewModel.name
        priceLabel.text = String(format: "Price: $%.02f", cellViewModel.price)
        ratingLabel.text = String(format: "Rating: %.01f", cellViewModel.rating)
        descriptionLabel.text = cellViewModel.description
    }
    
    func configureImageViews(with items: [ImageItem]) {
        let imageWidth = imageScrollView.bounds.width
        let imageHeight = imageScrollView.bounds.height
        for (index, item) in items.enumerated() {
            let rect = CGRect(
                x: CGFloat(index) * imageWidth,
                y: CGFloat(0),
                width: imageWidth,
                height: imageHeight
            )
            let imageView = UIImageView(frame: rect)
            imageView.image(
                from: item.imageURL,
                placeholderImage: UIImage(named: "placeholder_image")
            )
            imageView.contentMode = .scaleAspectFit
            imageScrollView.addSubview(imageView)
        }
        imageScrollView.contentSize = CGSize(
            width: CGFloat(items.count) * imageWidth,
            height: imageHeight
        )
    }
}
