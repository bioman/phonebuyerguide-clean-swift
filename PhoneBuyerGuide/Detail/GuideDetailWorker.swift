//
//  GuideDetailWorker.swift
//  PhoneBuyerGuide
//
//  Created by Khanit on 29/5/2562 BE.
//  Copyright (c) 2562 SCB. All rights reserved.
//

protocol GuideDetailStoreProtocol {
    func fetchPhoneDetail(with phoneId: Int64, completion: @escaping PhoneDetailServiceCompletion)
}

final class GuideDetailWorker {
    
    var store: GuideDetailStoreProtocol
    
    init(store: GuideDetailStoreProtocol) {
        self.store = store
    }
    
    // MARK: - Business Logic
    
    func fetchPhoneDetail(with phoneId: Int64, completion: @escaping (PhoneDetailServiceCompletion)) {
        // NOTE: Do the work
        store.fetchPhoneDetail(with: phoneId) {
            completion($0)
        }
    }
}

