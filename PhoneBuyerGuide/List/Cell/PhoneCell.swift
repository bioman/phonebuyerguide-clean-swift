//
//  PhoneCell.swift
//  PhoneBuyerGuide
//
//  Created by Khanitvidthayanon, Khanit (Agoda) on 30/3/2562 BE.
//  Copyright © 2562 Khanitvidthayanon, Khanit. All rights reserved.
//

import UIKit

protocol PhoneCellOutput: class {
    func markFavorite(phoneId: Int64)
    func removeFavorite(phoneId: Int64)
}

final class PhoneCell: UITableViewCell {

    private var phoneId: Int64?
    @IBOutlet private weak var phoneImageView: UIImageView!
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var priceLabel: UILabel!
    @IBOutlet private weak var ratingLabel: UILabel!
    @IBOutlet private weak var favoriteButton: UIButton!

    weak var output: PhoneCellOutput?

    public static let reuseIdentifier = "PhoneCell"

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func configure(with viewModel: PhoneCellViewModel, pageType: PageType) {
        phoneId = viewModel.phoneId
        nameLabel.text = viewModel.name
        descriptionLabel.text = viewModel.description
        priceLabel.text = String(format: "Price: $%.02f", viewModel.price)
        ratingLabel.text = String(format: "Rating: %.01f", viewModel.rating)
        favoriteButton.isSelected = viewModel.isFavorite
        favoriteButton.isHidden = pageType == .favorite ? true : false
        phoneImageView.image(
            from: viewModel.imageURL,
            placeholderImage: UIImage(named: "placeholder_image")
        )
    }

    @IBAction func markFavoriteButtonTapped(_ sender: UIButton) {
        guard let phoneId = phoneId else { return }
        sender.isSelected = !sender.isSelected
        if (sender.isSelected) {
            output?.markFavorite(phoneId: phoneId)
        } else {
            output?.removeFavorite(phoneId: phoneId)
        }
    }
}
