//
//  PhoneCellViewModel.swift
//  PhoneBuyerGuide
//
//  Created by Khanitvidthayanon, Khanit (Agoda) on 30/3/2562 BE.
//  Copyright © 2562 Khanitvidthayanon, Khanit. All rights reserved.
//

final class PhoneCellViewModel {
    let phoneId: Int64
    let imageURL: String
    let name: String
    let description: String
    let price: Double
    let rating: Double
    var isFavorite: Bool

    init(phoneId: Int64,
         imageURL: String,
         name: String,
         description: String,
         price: Double,
         rating: Double,
         isFavorite: Bool) {
        self.phoneId = phoneId
        self.imageURL = imageURL
        self.name = name
        self.description = description
        self.price = price
        self.rating = rating
        self.isFavorite = isFavorite
    }
}

extension PhoneCellViewModel: Equatable {
    static func == (lhs: PhoneCellViewModel, rhs: PhoneCellViewModel) -> Bool {
        return lhs.phoneId == rhs.phoneId &&
        lhs.imageURL == rhs.imageURL &&
        lhs.name == rhs.name &&
        lhs.description == rhs.description &&
        lhs.price == rhs.price &&
        lhs.rating == rhs.rating &&
        lhs.isFavorite == rhs.isFavorite
    }
}
