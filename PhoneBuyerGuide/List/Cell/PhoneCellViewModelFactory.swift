//
//  PhoneCellViewModelFactory.swift
//  PhoneBuyerGuide
//
//  Created by Khanitvidthayanon, Khanit (Agoda) on 30/3/2562 BE.
//  Copyright © 2562 Khanitvidthayanon, Khanit. All rights reserved.
//

import UIKit

final class PhoneCellViewModelFactory {
    static func buildCellViewModels(from items: [PhoneItem], favoriteList: [Int64]?) -> [PhoneCellViewModel] {
        return items.map {
            let isFavorite = favoriteList?.contains($0.phoneId)
            return PhoneCellViewModel(
                phoneId: $0.phoneId,
                imageURL: $0.thumbImageURL,
                name: $0.name,
                description: $0.description,
                price: $0.price,
                rating: $0.rating,
                isFavorite: isFavorite ?? false
            )
        }
    }
}
