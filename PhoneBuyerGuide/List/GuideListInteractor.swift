//
//  GuideListInteractor.swift
//  PhoneBuyerGuide
//
//  Created by Khanit on 28/5/2562 BE.
//  Copyright (c) 2562 SCB. All rights reserved.
//

protocol GuideListInteractorInterface {
    func viewIsReady()
    func reloadData()
    func presentSortOptions() -> SortOptions.Completion
    func select(pageType: PageType)
    func removeFavorite(phoneId: Int64)
}

final class GuideListInteractor: GuideListInteractorInterface {
    
    var presenter: GuideListPresenterInterface!
    var worker: GuideListWorker?
    private var model: [PhoneItem]?
    
    // MARK: - Business logic
    
    func viewIsReady() {
        fetchPhoneList()
    }
    
    func reloadData() {
        fetchPhoneList()
    }
    
    func select(pageType: PageType) {
        switch pageType {
        case .all: presenter.showAll()
        case .favorite: presenter.showFavorite()
        }
    }
    
    func presentSortOptions() -> SortOptions.Completion {
        return { decision in
            switch decision {
            case .priceLowToHigh:
                self.presenter.sortPriceLowToHigh()
            case .priceHighToLow:
                self.presenter.sortPriceHighToLow()
            case .rating:
                self.presenter.sortRating()
            case .cancel:
                break
            }
        }
    }
}

private extension GuideListInteractor {
    func fetchPhoneList() {
        worker?.fetchPhoneList { [weak self] in
            if case let Result.success(data) = $0 {
                self?.model = data
            } else if case Result.failure(_) = $0 {
                // present error alert message
                return
            }
            
            let response = GuideList.FetchList.Response(
                phoneItem: self?.model,
                favouriteList: self?.worker?.fetchFavouriteList()
            )
            self?.presenter.showResult(response: response)
        }
    }
}

extension GuideListInteractor: PhoneCellOutput {
    func markFavorite(phoneId: Int64) {
        worker?.store.markFavorite(phoneId: phoneId)
        presenter.markFavorite(phoneId: phoneId)
    }
    
    func removeFavorite(phoneId: Int64) {
        worker?.store.removeFavorite(phoneId: phoneId)
        presenter.removeFavorite(phoneId: phoneId)
    }
}
