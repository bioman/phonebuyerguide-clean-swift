//
//  GuideListModels.swift
//  PhoneBuyerGuide
//
//  Created by Khanit on 28/5/2562 BE.
//  Copyright (c) 2562 SCB. All rights reserved.
//

import UIKit

struct GuideList {
    /// This structure represents a use case
    struct FetchList {
        /// Data struct sent to Interactor
        struct Request {}
        /// Data struct sent to Presenter
        struct Response {
            let phoneItem: [PhoneItem]?
            let favouriteList: [Int64]?
        }
        /// Data struct sent to ViewController
        struct ViewModel {
            let phoneCellViewModel: [PhoneCellViewModel]?
        }
    }
}
