//
//  GuideListPresenter.swift
//  PhoneBuyerGuide
//
//  Created by Khanit on 28/5/2562 BE.
//  Copyright (c) 2562 SCB. All rights reserved.
//

protocol GuideListPresenterInterface {
    func showAll()
    func showFavorite()
    func sortPriceLowToHigh()
    func sortPriceHighToLow()
    func sortRating()
    func markFavorite(phoneId: Int64)
    func removeFavorite(phoneId: Int64)
    func showResult(response: GuideList.FetchList.Response)
}

final class GuideListPresenter: GuideListPresenterInterface {
    weak var viewController: GuideListViewControllerInterface!
    var viewModels: [PhoneCellViewModel]?
    var currentPageType: PageType = .all
    
    // MARK: - Presentation logic

    func showAll() {
        currentPageType = .all
        reloadView(with: viewModels)
    }
    
    func showFavorite() {
        currentPageType = .favorite
        reloadView(with: viewModels)
    }
    
    func filterFavorite() -> [PhoneCellViewModel]? {
        return viewModels?.filter({ $0.isFavorite == true })
    }
    
    func sortPriceLowToHigh() {
        viewModels?.sort { $0.price < $1.price }
        reloadView(with: viewModels)
    }
    
    func sortPriceHighToLow() {
        viewModels?.sort { $0.price > $1.price }
        reloadView(with: viewModels)
    }
    
    func sortRating() {
        viewModels?.sort { $0.rating > $1.rating }
        reloadView(with: viewModels)
    }
    
    func reloadView(with viewModels: [PhoneCellViewModel]?) {
        if currentPageType == .favorite {
            viewController.reloadPhoneList(viewModels: filterFavorite())
        } else {
            viewController.reloadPhoneList(viewModels: viewModels)
        }
    }

    func showResult(response: GuideList.FetchList.Response) {
        let viewModels = PhoneCellViewModelFactory.buildCellViewModels(
            from: response.phoneItem ?? [],
            favoriteList: response.favouriteList
        )
        self.viewModels = viewModels
        reloadView(with: viewModels)
    }
    
    func markFavorite(phoneId: Int64) {
        viewModels?.filter{ $0.phoneId == phoneId }.first?.isFavorite = true
    }
    
    func removeFavorite(phoneId: Int64) {
        viewModels?.filter{ $0.phoneId == phoneId }.first?.isFavorite = false
        if currentPageType == .favorite {
            showFavorite()
        }
    }
}

