//
//  GuideListRouter.swift
//  PhoneBuyerGuide
//
//  Created by Khanit on 28/5/2562 BE.
//  Copyright (c) 2562 SCB. All rights reserved.
//

import UIKit

protocol GuideListRouterInput {
    func presentPhoneDetail(with cellViewModel: PhoneCellViewModel)
    func presentSortOption(model: SortOptions.Model, completion: @escaping SortOptions.Completion)
}

final class GuideListRouter: GuideListRouterInput {

    weak var viewController: GuideListViewController!

    // MARK: - Navigation
    
    func presentPhoneDetail(with cellViewModel: PhoneCellViewModel) {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "GuideDetailViewController")
        
        guard let detailViewController = vc as? GuideDetailViewController else {
            fatalError("PhoneDetailViewController not found")
        }
        detailViewController.phoneCellViewModel = cellViewModel
        viewController.navigationController?.pushViewController(detailViewController, animated: true)
    }
    
    func presentSortOption(model: SortOptions.Model, completion: @escaping SortOptions.Completion) {
        let alert = SortOptions.sortOptions(model: model, completion: completion)
        viewController?.navigationController?.present(alert, animated: true, completion: nil)
    }
}

