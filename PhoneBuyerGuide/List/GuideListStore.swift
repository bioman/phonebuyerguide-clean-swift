//
//  GuideListStore.swift
//  PhoneBuyerGuide
//
//  Created by Khanit on 28/5/2562 BE.
//  Copyright (c) 2562 SCB. All rights reserved.
//

import Foundation

struct Constants {
    static let favoriteListKey = "FAVORITE_LIST"
}

final class GuideListStore: GuideListStoreProtocol {
    var service: PhoneService?
    var userDefault: UserDefaults?

    func fetchPhoneList(completion: @escaping PhoneServiceCompletion) {
        service?.fetchPhoneList(completion: completion)
    }
    
    func fetchFavouriteList() -> [Int64]? {
        return userDefault?.array(forKey: Constants.favoriteListKey) as? [Int64]
    }

    func markFavorite(phoneId: Int64) {
        if var favoriteList = userDefault?.array(forKey: Constants.favoriteListKey) as? [Int64] {
            if !favoriteList.contains(phoneId) {
                favoriteList.append(phoneId)
                userDefault?.set(favoriteList, forKey: Constants.favoriteListKey)
            }
        } else {
            let favoriteList: Array<Int64> = [phoneId]
            userDefault?.set(favoriteList, forKey: Constants.favoriteListKey)
        }
    }
    
    func removeFavorite(phoneId: Int64) {
        if var favoriteList = userDefault?.array(forKey: Constants.favoriteListKey) as? [Int64] {
            if favoriteList.contains(phoneId) {
                if let index = favoriteList.index(of: phoneId) {
                    favoriteList.remove(at: index)
                    userDefault?.set(favoriteList, forKey: Constants.favoriteListKey)
                }
            }
        }
    }
}
