//
//  GuideListViewController.swift
//  PhoneBuyerGuide
//
//  Created by Khanit on 28/5/2562 BE.
//  Copyright (c) 2562 SCB. All rights reserved.
//

import UIKit

enum PageType: Int {
    case all
    case favorite
}

protocol GuideListViewControllerInterface: class {
    func reloadPhoneList(viewModels: [PhoneCellViewModel]?)
}

final class GuideListViewController: UITableViewController, GuideListViewControllerInterface {
    var interactor: GuideListInteractorInterface!
    var router: GuideListRouter!
    private var cellViewModels: [PhoneCellViewModel] = []
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    // MARK: - Object lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configure(viewController: self)
    }
    
    // MARK: - Configuration
    
    private func configure(viewController: GuideListViewController) {
        let router = GuideListRouter()
        router.viewController = viewController
        
        let presenter = GuideListPresenter()
        presenter.viewController = viewController
        
        let interactor = GuideListInteractor()
        interactor.presenter = presenter
        
        let store = GuideListStore()
        store.service = PhoneServiceImplementation()
        store.userDefault = UserDefaults.standard
        
        interactor.worker = GuideListWorker(store: store)
        
        viewController.interactor = interactor
        viewController.router = router
    }
    
    // MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        interactor.viewIsReady()
    }
    
    // MARK: - Initialize views
    
    private func setupTableView() {
        tableView.tableFooterView = UIView()
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(
            self,
            action: #selector(handleRefresh(_:)),
            for: UIControl.Event.valueChanged
        )
        refreshControl?.layoutIfNeeded()
        refreshControl?.beginRefreshing()
    }
    
    // MARK: - Event handling
    
    @IBAction func segmentedTapped(_ sender: UISegmentedControl) {
        interactor.select(pageType: PageType.init(rawValue: sender.selectedSegmentIndex) ?? .all)
    }

    @IBAction func sortButtonTapped(_ sender: Any) {
        let optionCompletion = interactor.presentSortOptions()
        let sortModel = SortOptions.Model(
            title: "Sort",
            message: "",
            priceLowToHighCaption: "Price low to high",
            priceHighToLowCaption: "Price high to low",
            ratingCaption: "Rating",
            cancelCaption: "Cancel"
        )
        router.presentSortOption(model: sortModel, completion: optionCompletion)
    }

    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        refreshControl.beginRefreshing()
        interactor.reloadData()
    }
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellViewModels.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: PhoneCell.reuseIdentifier) else {
            fatalError("Can not dequeue cell")
        }
        
        if let cell = cell as? PhoneCell {
            let cellViewModel = cellViewModels[indexPath.row]
            cell.configure(
                with: cellViewModel,
                pageType: PageType(rawValue: segmentedControl.selectedSegmentIndex) ?? .all
            )
            cell.output = interactor as? PhoneCellOutput
        }
        return cell
    }
    
    // MARK: - Table view delegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        router.presentPhoneDetail(with: cellViewModels[indexPath.row])
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if let pageType = PageType.init(rawValue: segmentedControl.selectedSegmentIndex),
            pageType == .favorite {
            return true
        }
        return false
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            let phoneId = cellViewModels[indexPath.row].phoneId
            interactor.removeFavorite(phoneId: phoneId)
        }
    }
    
    // MARK: - Display logic
    
    func reloadPhoneList(viewModels: [PhoneCellViewModel]?) {
        guard let cellViewModels = viewModels else { return }
        self.cellViewModels = cellViewModels
        DispatchQueue.main.async {
            self.tableView.reloadData()
            self.refreshControl?.endRefreshing()
        }
    }
}

