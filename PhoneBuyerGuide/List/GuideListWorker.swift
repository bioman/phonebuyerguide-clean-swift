//
//  GuideListWorker.swift
//  PhoneBuyerGuide
//
//  Created by Khanit on 28/5/2562 BE.
//  Copyright (c) 2562 SCB. All rights reserved.
//

import UIKit

protocol GuideListStoreProtocol {
    func fetchPhoneList(completion: @escaping PhoneServiceCompletion)
    func fetchFavouriteList() -> [Int64]?
    func markFavorite(phoneId: Int64)
    func removeFavorite(phoneId: Int64)
}

final class GuideListWorker {
    
    var store: GuideListStoreProtocol
    
    init(store: GuideListStoreProtocol) {
        self.store = store
    }
    
    // MARK: - Business Logic
    
    func fetchPhoneList(_ completion: @escaping (PhoneServiceCompletion)) {
        store.fetchPhoneList {
            completion($0)
        }
    }
    
    func fetchFavouriteList() -> [Int64] {
        return store.fetchFavouriteList() ?? []
    }
}
