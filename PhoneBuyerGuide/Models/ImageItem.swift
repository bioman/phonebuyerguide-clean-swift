//
//  ImageItem.swift
//  PhoneBuyerGuide
//
//  Created by Khanitvidthayanon, Khanit (Agoda) on 31/3/2562 BE.
//  Copyright © 2562 Khanitvidthayanon, Khanit. All rights reserved.
//

struct ImageItem: Codable {
    let mobileId: Int64
    let imageId: Int64
    let imageURL: String

    public enum CodingKeys: String, CodingKey {
        case mobileId = "mobile_id"
        case imageId = "id"
        case imageURL = "url"
    }
}
