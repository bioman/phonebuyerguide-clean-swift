//
//  PhoneItem.swift
//  PhoneBuyerGuide
//
//  Created by Khanitvidthayanon, Khanit (Agoda) on 30/3/2562 BE.
//  Copyright © 2562 Khanitvidthayanon, Khanit. All rights reserved.
//

struct PhoneItem: Codable {
    let phoneId: Int64
    let thumbImageURL: String
    let name: String
    let brand: String
    let price: Double
    let rating: Double
    let description: String

    public enum CodingKeys: String, CodingKey {
        case phoneId = "id"
        case thumbImageURL = "thumbImageURL"
        case name = "name"
        case brand = "brand"
        case price = "price"
        case rating = "rating"
        case description = "description"
    }
}
