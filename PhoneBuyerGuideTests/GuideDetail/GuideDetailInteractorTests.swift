//
//  GuideDetailInteractorTests.swift
//  PhoneBuyerGuideTests
//
//  Created by Khanit on 31/5/2562 BE.
//  Copyright © 2562 SCB. All rights reserved.
//

@testable
import PhoneBuyerGuide
import XCTest

final class GuideDetailInteractorTests: XCTestCase {

    private var interactor: GuideDetailInteractor!
    private var mockWorker: GuideDetailWorker!
    private var mockStore: GuideDetailStoreMock!
    private var mockPresenter: GuideDetailPresenterMock!

    override func setUp() {
        interactor = GuideDetailInteractor()
        mockStore = GuideDetailStoreMock()
        mockWorker = GuideDetailWorker(store: mockStore)
        mockPresenter = GuideDetailPresenterMock()
        interactor.presenter = mockPresenter
        interactor.worker = mockWorker
    }

    override func tearDown() {
        interactor = nil
        mockStore = nil
        mockWorker = nil
        mockPresenter = nil
    }

    func testFetchPhoneDetailSuccess() {
        // given
        let request = GuideDetail.Something.Request(phoneId: 1)
        mockStore.mockResult = GuideDetailSceneMock.MockSuccessRequest
        
        // when
        interactor.fetchPhoneDetail(with: request)
        
        // then
        XCTAssertTrue(mockStore.fetchPhoneDetailCalled)
        XCTAssertTrue(mockPresenter.presentImagesCalled)
    }
    
    func testFetchPhoneDetailFailed() {
        // given
        let request = GuideDetail.Something.Request(phoneId: 1)
        mockStore.mockResult = GuideDetailSceneMock.MockFailedRequest
        
        // when
        interactor.fetchPhoneDetail(with: request)
        
        // then
        XCTAssertTrue(mockStore.fetchPhoneDetailCalled)
        XCTAssertFalse(mockPresenter.presentImagesCalled)
    }
}
