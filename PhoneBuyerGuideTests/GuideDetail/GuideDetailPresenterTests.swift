//
//  GuideDetailPresenterTests.swift
//  PhoneBuyerGuideTests
//
//  Created by Khanit on 31/5/2562 BE.
//  Copyright © 2562 SCB. All rights reserved.
//

@testable
import PhoneBuyerGuide
import XCTest

final class GuideDetailPresenterTests: XCTestCase {

    var presenter: GuideDetailPresenter!
    var mockViewController: GuideDetailViewControllerMock!

    override func setUp() {
        presenter = GuideDetailPresenter()
        mockViewController = GuideDetailViewControllerMock()
        presenter.viewController = mockViewController
    }
    
    override func tearDown() {
        presenter = nil
        mockViewController = nil
    }


    func testPresentImages() {
        // given
        let imageItems = GuideDetailSceneMock.mockImageItems()
        
        // when
        presenter.presentImages(response: GuideDetail.Something.Response(images: imageItems))
        
        // then
        XCTAssertTrue(mockViewController.setupImageViewsCalled)
    }

}
