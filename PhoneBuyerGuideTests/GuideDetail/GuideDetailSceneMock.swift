//
//  GuideDetailSceneMock.swift
//  PhoneBuyerGuideTests
//
//  Created by Khanit on 31/5/2562 BE.
//  Copyright © 2562 SCB. All rights reserved.
//

@testable
import PhoneBuyerGuide
import Foundation

final class GuideDetailSceneMock {
    static func mockImageItems() -> [ImageItem] {
        return [
            ImageItem(mobileId: 1, imageId: 1, imageURL: "https://www.test.com"),
            ImageItem(mobileId: 2, imageId: 2, imageURL: "https://www.test.com"),
            ImageItem(mobileId: 3, imageId: 3, imageURL: "https://www.test.com")
        ]
    }
    static let MockSuccessRequest: (Result<[ImageItem]?>) = Result.success(mockImageItems())
    static let MockFailedRequest: (Result<[ImageItem]?>) = Result.failure(MockError())
}

final class GuideDetailViewControllerMock: GuideDetailViewControllerInterface {
    var setupImageViewsCalled = false
    func setupImageViews(with items: [ImageItem]?) {
        setupImageViewsCalled = true
    }
}

final class GuideDetailStoreMock: GuideDetailStoreProtocol {
    var fetchPhoneDetailCalled = false
    var mockResult: Result<[ImageItem]?> = GuideDetailSceneMock.MockFailedRequest
    func fetchPhoneDetail(with phoneId: Int64, completion: @escaping PhoneDetailServiceCompletion) {
        fetchPhoneDetailCalled = true
        completion(mockResult)
    }
}

final class GuideDetailPresenterMock: GuideDetailPresenterInterface {
    var presentImagesCalled = false
    func presentImages(response: GuideDetail.Something.Response) {
        presentImagesCalled = true
    }
}
