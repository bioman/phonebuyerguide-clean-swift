//
//  GuideListInteractorTests.swift
//  PhoneBuyerGuideTests
//
//  Created by Khanit on 30/5/2562 BE.
//  Copyright © 2562 SCB. All rights reserved.
//

@testable
import PhoneBuyerGuide
import XCTest

final class GuideListInteractorTests: XCTestCase {
    
    private var interactor: GuideListInteractor!
    private var mockWorker: GuideListWorker!
    private var mockStore: GuideListStoreMock!
    private var mockPresenter: GuideListPresenterMock!

    override func setUp() {
        interactor = GuideListInteractor()
        mockPresenter = GuideListPresenterMock()
        mockStore = GuideListStoreMock()
        mockWorker = GuideListWorker(store: mockStore)
        interactor.presenter = mockPresenter
        interactor.worker = mockWorker
    }

    override func tearDown() {
        interactor = nil
        mockWorker = nil
        mockStore = nil
        mockPresenter = nil
    }

    func testViewIsReady() {
        // When
        interactor.viewIsReady()
        
        // Then
        XCTAssertTrue(mockStore.fetchPhoneListCalled)
    }
    
    func testReloadDataSuccess() {
        // given
        mockStore.mockResult = GuideListSceneMock.MockSuccessRequest
        
        // when
        interactor.reloadData()
        
        // then
        XCTAssertTrue(mockStore.fetchPhoneListCalled)
        XCTAssertTrue(mockPresenter.showResultCalled)
    }
    
    func testReloadDataFailed() {
        // given
        mockStore.mockResult = GuideListSceneMock.MockFailedRequest
        
        // when
        interactor.reloadData()
        
        // then
        XCTAssertTrue(mockStore.fetchPhoneListCalled)
        XCTAssertFalse(mockPresenter.showResultCalled)
    }

    func testMarkFavorite() {
        // when
        interactor.markFavorite(phoneId: 1)
        
        // then
        XCTAssertTrue(mockStore.markFavoriteCalled)
        XCTAssertTrue(mockPresenter.markFavoriteCalled)
    }
    
    func testRemoveFavorite() {
        // when
        interactor.removeFavorite(phoneId: 1)
        
        // then
        XCTAssertTrue(mockStore.removeFavoriteCalled)
        XCTAssertTrue(mockPresenter.removeFavoriteCalled)
    }
}
