//
//  GuideListPresenterTests.swift
//  PhoneBuyerGuideTests
//
//  Created by Khanit on 31/5/2562 BE.
//  Copyright © 2562 SCB. All rights reserved.
//

@testable
import PhoneBuyerGuide
import XCTest

final class GuideListPresenterTests: XCTestCase {
    
    var presenter: GuideListPresenter!
    var mockViewController: GuideListViewControllerMock!

    override func setUp() {
        presenter = GuideListPresenter()
        mockViewController = GuideListViewControllerMock()
        presenter.viewController = mockViewController
    }

    override func tearDown() {
        presenter = nil
        mockViewController = nil
    }

    func testShowAll() {
        // when
        presenter.showAll()
        
        // then
        XCTAssertEqual(presenter.currentPageType, .all)
        XCTAssertTrue(mockViewController.reloadPhoneListCalled)
    }
    
    func testShowFavorite() {
        // when
        presenter.showFavorite()

        // then
        XCTAssertEqual(presenter.currentPageType, .favorite)
        XCTAssertTrue(mockViewController.reloadPhoneListCalled)
    }
    
    func testFilterFavorite() {
        // given
        presenter.viewModels = [
            GuideListSceneMock.mockPhoneCellViewModel(phoneId: 1, isFavorite: true),
            GuideListSceneMock.mockPhoneCellViewModel(phoneId: 2, isFavorite: false),
            GuideListSceneMock.mockPhoneCellViewModel(phoneId: 3, isFavorite: true)
        ]
        
        // when
        let filteredList = presenter.filterFavorite()
        
        // then
        XCTAssertTrue(filteredList?.count == 2)
    }
    
    func testPriceLowToHigh() {
        // when
        presenter.sortPriceLowToHigh()
        
        // then
        XCTAssertTrue(mockViewController.reloadPhoneListCalled)
    }
    
    func testPriceHighToLow() {
        // when
        presenter.sortPriceHighToLow()
        
        // then
        XCTAssertTrue(mockViewController.reloadPhoneListCalled)
    }
    
    func testSortRating() {
        // when
        presenter.sortRating()
        
        // then
        XCTAssertTrue(mockViewController.reloadPhoneListCalled)
    }
    
    func testMarkFavorite() {
        // given
        presenter.viewModels = [
            GuideListSceneMock.mockPhoneCellViewModel(phoneId: 1, isFavorite: true),
            GuideListSceneMock.mockPhoneCellViewModel(phoneId: 2, isFavorite: false),
            GuideListSceneMock.mockPhoneCellViewModel(phoneId: 3, isFavorite: true)
        ]

        // when
        presenter.markFavorite(phoneId: 2)
        let filteredList = presenter.filterFavorite()
        
        // then
        XCTAssertTrue(filteredList?.count == 3)
    }
    
    func testRemoveFavorite() {
        // given
        presenter.viewModels = [
            GuideListSceneMock.mockPhoneCellViewModel(phoneId: 1, isFavorite: true),
            GuideListSceneMock.mockPhoneCellViewModel(phoneId: 2, isFavorite: false),
            GuideListSceneMock.mockPhoneCellViewModel(phoneId: 3, isFavorite: true)
        ]

        // when
        presenter.removeFavorite(phoneId: 1)
        let filteredList = presenter.filterFavorite()
        
        // then
        XCTAssertTrue(filteredList?.count == 1)
    }
}
