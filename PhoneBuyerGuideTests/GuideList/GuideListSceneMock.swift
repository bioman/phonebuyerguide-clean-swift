//
//  GuidelistSceneMock.swift
//  PhoneBuyerGuideTests
//
//  Created by Khanit on 30/5/2562 BE.
//  Copyright © 2562 SCB. All rights reserved.
//

@testable
import PhoneBuyerGuide
import Foundation

struct MockError: Error {
    
}

final class GuideListSceneMock {
    static func mockPhoneItem() -> PhoneItem {
        return PhoneItem(
            phoneId: 99,
            thumbImageURL: "https://www.test.com",
            name: "iPhone",
            brand: "Apple",
            price: 399.0,
            rating: 8.5,
            description: "iPhone XXX"
        )
    }

    static func mockPhoneCellViewModel(
        phoneId: Int64,
        price: Double? = 399.0,
        rating: Double? = 8.5,
        isFavorite: Bool? = false) -> PhoneCellViewModel {
        return PhoneCellViewModel(
            phoneId: phoneId,
            imageURL: "https://www.test.com",
            name: "Phone",
            description: "Phone XXX",
            price: price ?? 0.0,
            rating: rating ?? 0.0,
            isFavorite: isFavorite ?? false
        )
    }

    static let MockSuccessRequest: (Result<[PhoneItem]?>) = Result.success([mockPhoneItem()])
    static let MockFailedRequest: (Result<[PhoneItem]?>) = Result.failure(MockError())
}

final class GuideListViewControllerMock: GuideListViewControllerInterface {
    var reloadPhoneListCalled = false
    func reloadPhoneList(viewModels: [PhoneCellViewModel]?) {
        reloadPhoneListCalled = true
    }
}

final class GuideListPresenterMock: GuideListPresenterInterface {
    
    var showAllCalled = false
    func showAll() {
        showAllCalled = true
    }
    
    var showFavoriteCalled = false
    func showFavorite() {
        showFavoriteCalled = true
    }
    
    var sortPriceLowToHighCalled = false
    func sortPriceLowToHigh() {
        sortPriceLowToHighCalled = true
    }
    
    var sortPriceHighToLowCalled = false
    func sortPriceHighToLow() {
        sortPriceHighToLowCalled = true
    }
    
    var sortRatingCalled = false
    func sortRating() {
        sortRatingCalled = true
    }
    
    var markFavoriteCalled = false
    func markFavorite(phoneId: Int64) {
        markFavoriteCalled = true
    }
    
    var removeFavoriteCalled = false
    func removeFavorite(phoneId: Int64) {
        removeFavoriteCalled = true
    }
    
    var showResultCalled = false
    func showResult(response: GuideList.FetchList.Response) {
        showResultCalled = true
    }
}

final class GuideListStoreMock: GuideListStoreProtocol {
    var fetchPhoneListCalled = false
    var mockResult: Result<[PhoneItem]?> = GuideListSceneMock.MockFailedRequest
    func fetchPhoneList(completion: @escaping PhoneServiceCompletion) {
        fetchPhoneListCalled = true
        completion(mockResult)
    }
    
    var fetchFavouriteListCalled = false
    var favaoriteListMock: [Int64]? = []
    func fetchFavouriteList() -> [Int64]? {
        fetchFavouriteListCalled = true
        return favaoriteListMock
    }
    
    var markFavoriteCalled = false
    func markFavorite(phoneId: Int64) {
        markFavoriteCalled = true
    }
    
    var removeFavoriteCalled = false
    func removeFavorite(phoneId: Int64) {
        removeFavoriteCalled = true
    }
}
